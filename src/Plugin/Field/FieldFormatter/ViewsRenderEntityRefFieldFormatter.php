<?php

namespace Drupal\view_entity_ref_field_formatter\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceFormatterBase;
use Drupal\views\Views;

/**
 * Plugin 'view_entity_ref_field_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "view_entity_ref_field_formatter",
 *   label = @Translation("Views render entity ref field formatter"),
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class ViewsRenderEntityRefFieldFormatter extends EntityReferenceFormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
        // Implement default settings.
        'view_id' => '',
        'display_id' => 'default',
      ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);
    $form['info'] = [
      '#markup' => 'Remember to set the views (advanced) contextual filter this entity id example "Taxonomy term: Term ID"',
    ];
    $form['view_id'] = [
      '#title' => t('View machine name'),
      '#type' => 'textfield',
      '#required' => TRUE,
      '#description' => 'This view must have an entity id contextual argument',
      '#default_value' => $this->getSetting('view_id'),
    ];

    $form['display_id'] = [
      '#title' => t('View display_id'),
      '#type' => 'textfield',
      '#required' => TRUE,
      '#description' => 'example: default, block',
      '#default_value' => $this->getSetting('display_id'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $vid = $this->getSetting('view_id');
    if (empty($vid)) {
      $vid = ' View id Not Set';
    }
    $summary = [];
    $summary[] = t('View id @vid dispaly as @dis', [
      '@vid' => $this->getSetting('view_id'),
      '@dis' => $this->getSetting('display_id'),
    ]);
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $view_id = $this->getSetting('view_id');
    $display_id = $this->getSetting('display_id');
    if (empty($view_id) || empty($display_id)) {
      throw new \Exception('View id is missing in entity ref formatter');
    }
    $elements = [];
    $entities = $this->getEntitiesToView($items, $langcode);
    if (!empty($entities)) {
      if (count($entities) == 1) {
        $entity = reset($entities);
        $args = [$entity->id()];
        $view = Views::getView($view_id);
        if (is_object($view)) {
          $view->setArguments($args);
          $view->setDisplay($display_id);
          $view->preExecute();
          $view->execute();
          $content = $view->buildRenderable($display_id, $args);
          $elements[] = $content;
        }
        else {
          throw new \Exception('View id is not valid in entity ref formatter.');
        }

      }
      else {
        $arguments = [];
        foreach ($entities as $delta => $entity) {
          // $elements[$delta] = ['#markup' => $entity->id()];
          $arguments[] = $entity->id();
        }
        if (!empty($arguments)) {
          // Multiple values.
          $args = [implode(',', $arguments)];
          $view = Views::getView($view_id);
          if (is_object($view)) {
            $view->setArguments($args);
            $view->setDisplay($display_id);
            $view->preExecute();
            $view->execute();
            $content = $view->buildRenderable($display_id, $args);
            $elements[] = $content;
          }
          else {
            throw new \Exception('View id is not valid in entity ref formatter.');
          }
        }
      }
    }
    return $elements;
  }

}
